const express = require('express');
const app = express();
const port = process.env.PORT || 80

app.get('/', (req,res) =>{
    res.send("Hello World from dev");
});

app.listen(port, ()=>{
    console.log(`APP listening at http://localhost:${port}`);
});